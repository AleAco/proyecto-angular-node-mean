# Productos

Durante la realización de este proyecto, se creo un CRUD (Create, Read, Update, Delete) completo utilizando el Stack MEAN.

Para ejecutar el ejemplo, basta correr desde el lado del **cliente**:
`ng serve`

Desde el **servidor**, ejecutar:
`npm run dev`

En la rama **auth**, se incluyó la autentificación con JWT.
